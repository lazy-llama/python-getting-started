# Python getting started

A project to get started with python again, to help me in the first few days of college.


## What Python can do:

* Automation
* Mathematiks (Numpy / matplotlib)
* AI (Tensorflow / SciKit Learn / Keras / Theano)
* Biology (Biopython)
* Astronomy (Astropy)
* GUI (TkInter)
* Web Frameworks (Django)
* Pentesting
* Games (PyGame)